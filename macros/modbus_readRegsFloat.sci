// Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
// Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
//
// This file must be used under the terms of the CeCILL.
// This source file is licensed as described in the file COPYING, which
// you should have received as part of this distribution.  The terms
// are also available at
// http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
//
//
function varargout = modbus_readRegsFloat(modbusContext, address, readCount, isBigEndian)
    [lhs, rhs] = argn(0);
    if lhs <> [1 2] then
        error(msprintf(gettext("%s: Wrong number of output argument(s): %d or %d expected.\n"),"modbus_writeBit", 1, 2));
    end

    if rhs <> 4 then
        error(msprintf(gettext("%s: Wrong number of input argument(s): %d expected.\n"),"modbus_writeBit", 4));
    end

    if modulo(readCount, 2) <> 0 then
        error(msprintf(_("%s:  Wrong value for input argument #%d: Must be even.\n"), "modbus_read_registers_float"), 3);
    end

    //read values from network as short (16 bits)
    [retValInt, ierr] = modbus_readRegs(modbusContext, address, readCount);
    varargout(2) = ierr;
    if ierr <> 0 then
        varargout(1) = [];
    else
        //convert two shorts (2 * 16 bits) to float (32 bits)
        varargout(1) = modbus_getFloat(retValInt, isBigEndian);
    end
endfunction
