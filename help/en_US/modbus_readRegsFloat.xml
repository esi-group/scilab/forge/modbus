<?xml version="1.0" encoding="UTF-8"?>
<refentry xmlns="http://docbook.org/ns/docbook" xmlns:xlink="http://www.w3.org/1999/xlink" xmlns:svg="http://www.w3.org/2000/svg" xmlns:mml="http://www.w3.org/1998/Math/MathML" xmlns:db="http://docbook.org/ns/docbook" xmlns:scilab="http://www.scilab.org" xml:lang="en" xml:id="modbus_readRegsFloat">
  <refnamediv>
    <refname>modbus_readRegsFloat</refname>

    <refpurpose>read many registers</refpurpose>
  </refnamediv>

  <refsynopsisdiv>
    <title>Calling Sequence</title>

    <synopsis>
res = modbus_readRegsFloat(mbc, addr, nb, isBigEndian)
[res, ierr] = modbus_readRegsFloat(mbc, addr, isBigEndian)
    </synopsis>
  </refsynopsisdiv>

  <refsection>
    <title>Arguments</title>
    <variablelist>
      <varlistentry>
        <term>mbc</term>
        <listitem>
          <para>The Modbus connection created with modbus_newTcp or modbus_newRtu.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>addr</term>
        <listitem>
          <para>The address on device where registers should be read from.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>nb</term>
        <listitem>
          <para>The number of holding registers that should be read.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>isBigEndian</term>
        <listitem>
          <para>Way to convert read data.
          BigEndian     : res = addr[0] &lt;&lt; 16 + addr[1] or
          LittelEndian  : res = addr[1] &lt;&lt; 16 + addr[0].</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>res</term>
        <listitem>
          <para>A double matrix (size 1 x nb / 2) with registers values. An empty matrix in case of error when called with two output arguments.</para>
        </listitem>
      </varlistentry>
      <varlistentry>
        <term>ierr</term>
        <listitem>
          <para>Erreur number.</para>
        </listitem>
      </varlistentry>
    </variablelist>
  </refsection>

  <refsection>
    <title>Description</title>

    <para>This function reads the status of the <literal>nb</literal> holding registers to
    the address <literal>addr</literal> of the remote device through <literal>mbc</literal> connection
    descriptor.</para>

    <para>The function uses the Modbus function code 0x03 (read holding registers).</para>

    <para>The function uses Modbus 2 unsigned short (16 bits) catenation to create float (32 bits) values.</para>

    <para>When called with one output argument, a Scilab error will occur when the reading fails. When called with two output arguments, no Scilab error will occur but <literal>ierr</literal> will contain the internal modbus error number.</para>

  </refsection>

  <refsection>
    <title>Examples</title>

    <programlisting role="example"><![CDATA[
    mbc = modbus_newTcp("127.0.0.1", 502);
    modbus_connect(mbc);
    res = modbus_readRegsFloat(mbc, 0, 8, %f);
    modbus_close(mbc);
    modbus_free(mbc);
    ]]></programlisting>
  </refsection>
  <refsection role="see also">
    <title>See Also</title>
    <simplelist type="inline">
      <member>
        <link linkend="modbus_newTcp">modbus_newTcp</link>
      </member>
      <member>
        <link linkend="modbus_connect">modbus_connect</link>
      </member>
      <member>
        <link linkend="modbus_writeRegs">modbus_writeRegs</link>
      </member>
    </simplelist>
  </refsection>
</refentry>
