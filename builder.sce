// Copyright (C) 2011 - DIGITEO - Allan CORNET
// Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS

mode(-1);
lines(0);

TOOLBOX_NAME  = "modbus";
TOOLBOX_TITLE = "modbus";
toolbox_dir   = get_absolute_file_path("builder.sce");

// Check Scilab"s version
// =============================================================================

try
    v = getversion("scilab");
catch
    error(gettext("Scilab 6.0 or more is required."));
end

if v(1) < 6 then
    // new API in scilab 6.0
    error(gettext("Scilab 6.0 or more is required."));
end
clear v;

// Check modules_manager module availability
// =============================================================================

if ~isdef("tbx_build_loader") then
  error(msprintf(gettext("%s module not installed."), "modules_manager"));
end

// Action
// =============================================================================
//tbx_builder_src(toolbox_dir);
tbx_builder_gateway(toolbox_dir);
tbx_builder_macros(toolbox_dir);
tbx_builder_help(toolbox_dir);
tbx_build_loader(TOOLBOX_NAME, toolbox_dir);
tbx_build_cleaner(TOOLBOX_NAME, toolbox_dir);

// Clean variables
// =============================================================================

clear toolbox_dir TOOLBOX_NAME TOOLBOX_TITLE;

