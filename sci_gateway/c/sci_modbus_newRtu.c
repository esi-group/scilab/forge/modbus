/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2011 - DIGITEO - Allan CORNET
 * Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <string.h>
#include <errno.h>

#include "modbus-tcp.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "sci_malloc.h"
#include "localization.h"

/* ========================================================================== */
int sci_modbus_newRtu(char *fname, void * pvApiCtx)
{
    modbus_t *pstrMod = NULL;
    SciErr sciErr;
    int *piAddr1 = NULL;
    int *piAddr2 = NULL;
    int *piAddr3 = NULL;
    int *piAddr4 = NULL;
    int *piAddr5 = NULL;

    char *pstDevice = NULL;

    double dblBaud = 0;
    int iBaud = 0;

    char *pstParity = NULL;
    double dblBits = 0;
    double dblStop = 0;

    CheckInputArgument(pvApiCtx, 5, 5);
    CheckOutputArgument(pvApiCtx, 1, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isStringType(pvApiCtx, piAddr1) || !isScalar(pvApiCtx, piAddr1))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 1);
        return 1;
    }

    if (getAllocatedSingleString(pvApiCtx, piAddr1, &pstDevice))
    {
        if (pstDevice)
        {
            freeAllocatedSingleString(pstDevice);
            pstDevice = NULL;
        }
        return 1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr2) || !isScalar(pvApiCtx, piAddr2))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 2);
        return 1;
    }

    if (getScalarDouble(pvApiCtx, piAddr2, &dblBaud))
    {
        return 1;
    }

    iBaud = (int)dblBaud;

    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddr3);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isStringType(pvApiCtx, piAddr3) || !isScalar(pvApiCtx, piAddr3))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A string expected.\n"), fname, 3);
        return 1;
    }

    if (getAllocatedSingleString(pvApiCtx, piAddr3, &pstParity))
    {
        return 1;
    }

    if (pstParity)
    {
        if ((int)strlen(pstParity) != 1)
        {
            if (pstDevice)
            {
                freeAllocatedSingleString(pstDevice);
                pstDevice = NULL;
            }
            if (pstParity)
            {
                freeAllocatedSingleString(pstParity);
                pstParity = NULL;
            }

            Scierror(999, _("%s: Wrong value for input argument #%d: A char expected.\n"), fname, 3);
            return 1;
        }
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 4, &piAddr4);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr4) || !isScalar(pvApiCtx, piAddr4))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 4);
        return 1;
    }

    if (getScalarDouble(pvApiCtx, piAddr4, &dblBits))
    {
        return 1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 5, &piAddr5);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr5) || !isScalar(pvApiCtx, piAddr5))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 5);
        return 1;
    }

    if (getScalarDouble(pvApiCtx, piAddr5, &dblStop))
    {
        return 1;
    }

    pstrMod = modbus_new_rtu(pstDevice, iBaud, pstParity[0], (int)dblBits, (int)dblStop);

    if (pstDevice)
    {
        freeAllocatedSingleString(pstDevice);
        pstDevice = NULL;
    }

    if (pstParity)
    {
        freeAllocatedSingleString(pstParity);
        pstParity = NULL;
    }

    sciErr = createPointer(pvApiCtx, nbInputArgument(pvApiCtx) + 1, (void*)pstrMod);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
    ReturnArguments(pvApiCtx);

    return 0;
}
/* ========================================================================== */
