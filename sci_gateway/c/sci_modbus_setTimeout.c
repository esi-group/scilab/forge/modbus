/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <stdlib.h>

#include "modbus-tcp.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "sci_malloc.h"
#include "localization.h"
/* ========================================================================== */
int sci_modbus_setTimeout(char *fname, void * pvApiCtx)
{
    SciErr sciErr;
    modbus_t *pstrMod = NULL;
    int *piAddr1 = NULL;
    int *piAddr2 = NULL;

    double dblTimeout = 0;
    int iTimeout = 0;
    struct timeval strTimeout;

    CheckInputArgument(pvApiCtx, 2, 2);
    CheckOutputArgument(pvApiCtx, 1, 1);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isPointerType(pvApiCtx, piAddr1))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A pointer expected.\n"), fname, 1);
        return 1;
    }

    sciErr = getPointer(pvApiCtx, piAddr1, (void**)&pstrMod);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (pstrMod == NULL)
    {
        Scierror(999, _("%s: Invalid pointer #%d.\n"), fname, 1);
        return 1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr2) || !isScalar(pvApiCtx, piAddr2))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 2);
        return 1;
    }

    if (getScalarDouble(pvApiCtx, piAddr2, &dblTimeout))
    {
        return 1;
    }

    iTimeout = (int)dblTimeout;

    strTimeout.tv_sec = iTimeout / 1000;
    strTimeout.tv_usec = (iTimeout - strTimeout.tv_sec*1000) * 1000;

    modbus_set_response_timeout(pstrMod, &strTimeout);
    ReturnArguments(pvApiCtx);

    return 0;
}
/* ========================================================================== */
