// Copyright 2010 - DIGITEO - Allan CORNET

function builder_gateway_c_modbus()

    gw_src_c = get_absolute_file_path("builder_gateway_c.sce");

    arch_modbus = "";

    if getos() == "Windows" then
// to manage long pathname
        includes_src_c = "-I""" + gw_src_c + "../../src/c""";
        if win64() then
            arch_modbus = "windows64"
        else
            arch_modbus = "windows32"
        end
    else
        includes_src_c = "-I" + gw_src_c + "../../src/c";
        if getos() == "Darwin" then
            arch_modbus = "macos64"
        else
            [version, opts] = getversion();
            if  opts(2) == "x64" then
                arch_modbus = "linux64"
            else
                arch_modbus = "linux32"
            end
        end
    end
    libmodbuspathinclude = " -I""" + gw_src_c + "/../../thirdparty/libmodbus/" + arch_modbus + "/includes""";
    includes_src_c = includes_src_c + libmodbuspathinclude;

// PutLhsVar managed by user
// if you do not this variable, PutLhsVar is added
// in gateway generated (default mode in scilab 4.x and 5.x)
    WITHOUT_AUTO_PUTLHSVAR = %t;

    functions_list = [ ..
        "modbus_close",             "sci_modbus_close"
        "modbus_connect",           "sci_modbus_connect"
        "modbus_flush",             "sci_modbus_flush"
        "modbus_free",              "sci_modbus_free"
        "modbus_getFloat",          "sci_modbus_getFloat"
        "modbus_getHeaderLength",   "sci_modbus_getHeaderLength"
        "modbus_getTimeout",        "sci_modbus_getTimeout"
        "modbus_newRtu",            "sci_modbus_newRtu"
        "modbus_newTcp",            "sci_modbus_newTcp"
        "modbus_readBits",          "sci_modbus_readBits"
        "modbus_readInBits",        "sci_modbus_readInBits"
        "modbus_readInRegs",        "sci_modbus_readInRegs"
        "modbus_readRegs",          "sci_modbus_readRegs"
        "modbus_setDebug",          "sci_modbus_setDebug"
        "modbus_setSlave",          "sci_modbus_setSlave"
        "modbus_setTimeout",        "sci_modbus_setTimeout"
        "modbus_tcpListen",         "sci_modbus_tcpListen"
        "modbus_writeBits",         "sci_modbus_writeBits"
        "modbus_writeRegs",         "sci_modbus_writeRegs"];

    files_list = [ ...
        "sci_modbus_close.c" ..
        "sci_modbus_connect.c" ..
        "sci_modbus_flush.c" ..
        "sci_modbus_free.c" ..
        "sci_modbus_getFloat.c" ..
        "sci_modbus_getHeaderLength.c" ..
        "sci_modbus_getTimeout.c" ..
        "sci_modbus_newRtu.c" ..
        "sci_modbus_newTcp.c" ..
        "sci_modbus_readBits.c" ..
        "sci_modbus_readInBits.c" ..
        "sci_modbus_readInRegs.c" ..
        "sci_modbus_readRegs.c" ..
        "sci_modbus_setDebug.c" ..
        "sci_modbus_setSlave.c" ..
        "sci_modbus_setTimeout.c" ..
        "sci_modbus_tcpListen.c" ..
        "sci_modbus_writeBits.c" ..
        "sci_modbus_writeRegs.c"];

    if getos() == "Windows" then
      files_list = [files_list, "dllMain.c"];
    end

    if getos() == "Windows" then
        files_list = [files_list, "dllMain.c"];
        tbx_build_gateway("gw_modbus", ..
                          functions_list, ..
                          files_list, ..
                          gw_src_c, ..
                          [], ..
                          "", ..
                          includes_src_c);
    else
        tbx_build_gateway("gw_modbus", ..
                          functions_list, ..
                          files_list, ..
			  gw_src_c, ..
			  ["../../thirdparty/libmodbus/" + arch_modbus + "/lib/libmodbus"], ..
                          ["-L" + gw_src_c + "../../thirdparty/libmodbus/" + arch_modbus + "/lib -lmodbus"], ..
                          includes_src_c);
    end
endfunction

builder_gateway_c_modbus();
clear builder_gateway_c_modbus;
