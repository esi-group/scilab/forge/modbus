/*
 * Scilab ( http://www.scilab.org/ ) - This file is part of Scilab
 * Copyright (C) 2011 - DIGITEO - Allan CORNET
 * Copyright (C) 2012 - Scilab Enterprises - Antoine ELIAS
 *
 * This file must be used under the terms of the CeCILL.
 * This source file is licensed as described in the file COPYING, which
 * you should have received as part of this distribution.  The terms
 * are also available at
 * http://www.cecill.info/licences/Licence_CeCILL_V2-en.txt
 *
 */

#include <errno.h>

#include "modbus-tcp.h"
#include "api_scilab.h"
#include "Scierror.h"
#include "sci_malloc.h"
#include "localization.h"
#include "BOOL.h"
/* ========================================================================== */
int sci_modbus_writeRegs(char *fname, void * pvApiCtx)
{
    SciErr sciErr;
    int errsv = 0;
    modbus_t *pstrMod = NULL;
    int *piAddr1 = NULL;
    int *piAddr2 = NULL;
    int *piAddr3 = NULL;
    int iRows3 = 0;
    int iCols3 = 0;
    int iSize3 = 0;
    int iSize = 0;
    int i = 0;

    double dblAddr = 0;
    int iAddr = 0;

    double* pdblVal = NULL;
    uint16_t* psVal = NULL;

    double dErrReturned = 0;

    CheckInputArgument(pvApiCtx, 3, 3);
    CheckOutputArgument(pvApiCtx, 1, 2);

    sciErr = getVarAddressFromPosition(pvApiCtx, 1, &piAddr1);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isPointerType(pvApiCtx, piAddr1))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A pointer expected.\n"), fname, 1);
        return 1;
    }

    sciErr = getPointer(pvApiCtx, piAddr1, (void**)&pstrMod);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (pstrMod == NULL)
    {
        Scierror(999, _("%s: Invalid pointer #%d.\n"), fname, 1);
        return 1;
    }

    sciErr = getVarAddressFromPosition(pvApiCtx, 2, &piAddr2);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr2) || !isScalar(pvApiCtx, piAddr2))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A double expected.\n"), fname, 2);
        return 1;
    }

    if (getScalarDouble(pvApiCtx, piAddr2, &dblAddr))
    {
        return 1;
    }

    iAddr = (int)dblAddr;

    sciErr = getVarAddressFromPosition(pvApiCtx, 3, &piAddr3);
    if (sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    if (!isDoubleType(pvApiCtx, piAddr3))
    {
        Scierror(999, _("%s: Wrong type for input argument #%d: A integer expected.\n"), fname, 3);
        return 1;
    }

    sciErr = getMatrixOfDouble(pvApiCtx, piAddr3, &iRows3, &iCols3, &pdblVal);
    if(sciErr.iErr)
    {
        printError(&sciErr, 0);
        return 1;
    }

    iSize3 = iRows3 * iCols3;
    psVal = (uint16_t*)MALLOC(sizeof(uint16_t) * iSize3);
    for(i = 0 ; i < iSize3 ; i++)
    {
        psVal[i] = (uint16_t)pdblVal[i];
    }

    errno = 0;
    iSize = modbus_write_registers(pstrMod, iAddr, iSize3, psVal);
    errsv = errno;
    errno = 0;
    FREE(psVal);
    if(iSize == -1 && nbOutputArgument(pvApiCtx) != 2)
    {
        Scierror(999, "%s: %s(%d -> %d)\n", fname, modbus_strerror(errsv), iSize3, iSize);
        return 1;
    }
    else if (iSize == -1)
    {
        if (createEmptyMatrix(pvApiCtx, nbInputArgument(pvApiCtx) + 1))
        {
            Scierror(999, _("%s: Memory error.\n"), fname);
            return 1;
        }

        if (createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, errsv))
        {
            Scierror(999, _("%s: Memory error.\n"), fname);
            return 1;
        }

        AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
        AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;
        ReturnArguments(pvApiCtx);
        return 0;
    }

    if (createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 1, (double)iSize))
    {
        return 1;
    }

    AssignOutputVariable(pvApiCtx, 1) = nbInputArgument(pvApiCtx) + 1;
    if(nbOutputArgument(pvApiCtx) == 2)
    {
        if (createScalarDouble(pvApiCtx, nbInputArgument(pvApiCtx) + 2, errsv))
        {
            Scierror(999, _("%s: Memory error.\n"), fname);
            return 1;
        }
        AssignOutputVariable(pvApiCtx, 2) = nbInputArgument(pvApiCtx) + 2;
    }

    ReturnArguments(pvApiCtx);
    return 0;
}
/* ========================================================================== */
