/* ========================================================================== *//* ========================================================================== */
/* Allan CORNET */
/* DIGITEO 2010 */
/* ========================================================================== */
#include <Windows.h>
/* ========================================================================== */
#if _WIN64
#pragma comment(lib, "../../thirdparty/libmodbus/windows64/lib/libmodbus.lib")
#else
#pragma comment(lib, "../../thirdparty/libmodbus/windows32/lib/libmodbus.lib")
#endif
/* ========================================================================== */
int WINAPI DllMain (HINSTANCE hInstance , DWORD reason, PVOID pvReserved)
{
    switch (reason)
    {
    case DLL_PROCESS_ATTACH:
        break;
    case DLL_PROCESS_DETACH:
        break;
    case DLL_THREAD_ATTACH:
        break;
    case DLL_THREAD_DETACH:
        break;
    }
    return 1;
}
/* ========================================================================== */
